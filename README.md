# [![STAMP](images/stamp.png)](https://github.com/STAMP-project) project: Evaluation in Atos SUPERSEDE UC
This repository contains code, artefacts, configuration, and so on, developed for the evaluation of the STAMP techniques, tools and services in the context of the ATOS SUPERSEDE UC. 

Information about the SUPERSEDE platform can be found in the [Supersede Portal](https://www.supersede.eu/).

Repository Structure:
- docker: provides Docker image descriptors (i.e. Dockerfiles) for the different SUPERSEDE components and few Docker compose descriptors (i.e. yml files) that compose up the entire platform as an aggregation of the different component containers.

SUPERSEDE platform and all the code content of this repository is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)


Main contact: Jesús Gorroñogoitia <jesus.gorronogoitia@atos.net>

![Project funded by the European Union](images/european.union.logo.png)
